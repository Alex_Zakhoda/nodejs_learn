const fs = require('fs');
const path = require('path');

const p = path.join(
    path.dirname(process.mainModule.filename),
    'data',
    'cart.json'
);

module.exports = class Cart {
    constructor() {
        this.products = [];
        this.totalPrice = 0;
    }

    static addProduct(id, productPrice) {
        fs.readFile(p, (err, fileContent) => {
            let cart = {products: [], totalPrice: 0}
            if (!err) {
                cart = JSON.parse(fileContent);
            }
            const existingProductIndex = cart.products.findIndex(prod => prod.id === id);
            const existingProduct = cart.products[existingProductIndex];
            let updatedProduct;
            if (existingProduct) {
                updatedProduct = { ...existingProduct };
                updatedProduct.qty = updatedProduct.qty + 1;
                cart.products = [...cart.products];
                cart.products[existingProductIndex] = updatedProduct;
            } else {
                updatedProduct = { id: id, qty: 1 };
                cart.products = [...cart.products, updatedProduct]
            }
            cart.totalPrice = cart.totalPrice + productPrice;
            fs.writeFile(p, JSON.stringify(cart), (err) => {
                console.log('err', err)
            });
        })
    }

    static deleteProduct(id, productPrice, deleteByProduct) {
        fs.readFile(p, (err, fileContent) => {
            if(err) {
                return;
            }
            const updatedCart = {...JSON.parse(fileContent)};
            const existingProductIndex = updatedCart.products.findIndex(prod => prod.id === id);
            const product = updatedCart.products.find(prod => prod.id === id);
            if (!product) {
                return;
            }
            const productQty = product.qty;
            
            if(product.qty > 1 && deleteByProduct === true) {
                let updatedProduct;
                updatedProduct = { ...product };
                updatedProduct.qty = updatedProduct.qty - 1;
                updatedCart.products[existingProductIndex] = updatedProduct;
                updatedCart.totalPrice = updatedCart.totalPrice - productPrice;
                fs.writeFile(p, JSON.stringify(updatedCart), (err) => {
                    console.log('err', err)
                });
            } else {
                updatedCart.products = updatedCart.products.filter(prod => prod.id !== id)
                updatedCart.totalPrice = updatedCart.totalPrice - productPrice * productQty;
                fs.writeFile(p, JSON.stringify(updatedCart), (err) => {
                    console.log('err', err)
                });
            }
        });
    }

    static getCart(callback) {
        fs.readFile(p, (err, fileContent) => {
            const cart = JSON.parse(fileContent);
            if(!cart) return;
            if(err) {
                callback(null)
            } else {
                callback(cart)
            }
        });
    }
}